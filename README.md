<p align="center">
pacebox-elasticsearch 基于elasticsearch的扩展工具包
</p>
<p align="center">
-- 主页：<a href="http://mhuang.tech/pacebox-elasticsearch">http://mhuang.tech/pacebox-elasticsearch</a>  --
</p>
<p align="center">
    -- QQ群①:<a target="_blank" href="//shang.qq.com/wpa/qunwpa?idkey=6703688b236038908f6c89b732758d00104b336a3a97bb511048d6fdc674ca01"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="pacebox官方交流群①" title="pacebox官方交流群①"></a>
</p>
---------------------------------------------------------------------------------------------------------------------------------------------------------

## 简介
pacebox-elasticsearch 是一个基于elasticsearch和pacebox-core封装的便捷工具包、简单几行代码即可对elasticsearch进行操作


## 安装

### MAVEN
在pom.xml中加入
```
    <dependency>
        <groupId>tech.mhuang.pacebox</groupId>
        <artifactId>pacebox-elasticsearch</artifactId>
        <version>${laster.version}</version>
    </dependency>
```
### 非MAVEN
下载任意链接
- [Maven中央库1](https://repo1.maven.org/maven2/tech/mhuang/pacebox/pacebox-elasticsearch/)
- [Maven中央库2](http://repo2.maven.org/maven2/tech/mhuang/pacebox/pacebox-elasticsearch/)
> 注意
> pacebox只支持jdk1.8以上的版本