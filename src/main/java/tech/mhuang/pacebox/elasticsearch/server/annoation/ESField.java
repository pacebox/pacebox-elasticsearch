package tech.mhuang.pacebox.elasticsearch.server.annoation;

/**
 * ES字段注解保留
 *
 * @author mhuang
 * @since 1.0.0
 */
public @interface ESField {

}
