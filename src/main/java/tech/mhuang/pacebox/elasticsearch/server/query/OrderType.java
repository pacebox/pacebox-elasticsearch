package tech.mhuang.pacebox.elasticsearch.server.query;

/**
 * 支持的排序类型
 *
 * @author zhangxh
 * @since 1.0.0
 */
public enum OrderType {
    ASC, DESC;
}
